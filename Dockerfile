# choose the base image
FROM ubuntu:16.04

# Add metadata for you image
LABEL  MAINTAINER="youremail@domain.tld"

# Define the default working directory
WORKDIR /app

# Install depenedencies
RUN apt-get update -y && \
    apt-get install -y python-pip python-dev

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt

# Instal app dependencies
RUN pip install -r requirements.txt

COPY . /app

EXPOSE 5001

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]
