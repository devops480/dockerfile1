import logging
import os
from flask import Flask, redirect, url_for, request, render_template



app = Flask(__name__)

# client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
# db = client.tododb


logging.basicConfig(filename='logger/app.log', encoding='utf-8', level=logging.DEBUG)


@app.route('/')
def hello():
    app.logger.info('Processing default request')
    print("Hello from root path")
    return "welcome to the flask tutorials"

@app.route('/todo')
def todo():

    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/todo/new', methods=['POST'])
def new():

    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.tododb.insert_one(item_doc)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=True)
