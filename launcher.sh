#!/bin/bash

REDIS_HOST=redis-master
REDIS_PORT=6379
# then start main
for i in {1..5}
do
  rq worker --url redis://$(REDIS_HOST):$(REDIS_PORT)
  echo $i
  echo $REDIS_HOST
  echo $REDIS_PORT
  sleep 2
done &

python app.py